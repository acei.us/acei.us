// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT
// welcome.widget.js - acei.us - Homepage greeter
const greetings = ['Welcome to acei.us', 'Bienvenido a acei.us', '歡迎來到acei.us', 'kwa gwa acei.us;']
let selected_greeting = 0;

const welcome_loop = window.setInterval(function(){
    document.getElementById("welcome").setAttribute("class", "home-center tracking-in-expand")
    console.log("[CSS] now tracking inward: #welcome")

    if (greetings.length == (selected_greeting + 1)) {
        selected_greeting = 0; console.log("[Debug] selected_greeting = " + selected_greeting);
    } else {
        selected_greeting += 1; console.log("[Debug] selected_greeting = " + selected_greeting);
    }

    document.getElementById("welcome").innerHTML = greetings[selected_greeting];

    window.setTimeout(function(){
        console.log("[CSS] Reseting classes: #welcome");
        document.getElementById("welcome").setAttribute("class", "home-center")
    }, 1500);
}, 3000);

document.getElementById("welcome").innerHTML = greetings[0];
// @license-end