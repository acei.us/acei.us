// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT
// headerio.js - acei.us - Calculator Vue Component
export default {
    template: /*html*/`
    <section>
        <p>Current Stardate: {{stardate}}</p>
        <input v-model="stardate_input" type="date"/>
        <p>Your Stardate: {{stardate_input}}</p>
    </section>
    `,
    computed: {
        stardate: function () {
            const months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
            const date = new Date();
            let staryear = date.getFullYear()
            let starmonth = months[date.getMonth()]
            let timeindex = date.getDate()

            let final_stardate = staryear + '' + starmonth + '.' + timeindex
            return(final_stardate);
        },
    },
    name: 'stardate'
}
// @license-end