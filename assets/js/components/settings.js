// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT
// settings.js - acei.us - Settings Vue component
export default {
    template: /*html*/`<div id="settings" class="modal" style="display: none; color: black;">
        <div class="modal-content">
            <div class="container">
                <span onclick="document.getElementById('settings').style.display='none'"
                class="button display-topright red">&times;</span>
                <h3><i class="fa-solid fa-gear"></i> Settings</h3>
                <p><i class="fa-solid fa-circle-half-stroke"></i> Theme</p>
                <div>
                    <select class="select border" id="settingtheme">
                        <option value="dark" selected>Dark</option>
                        <option value="light">Light</option>
                        <option value="amongus" disabled>Secret</option>
                    </select>
                    <br /><br />
                    <button @click="set_theme" class="button green">Save</button>
                    <br /><br />
                </div>
            </div>
        </div>
    </div>`,
    methods: {
        set_theme: () => {
            const body = document.getElementById("app");
            let theme = document.getElementById("settingtheme").value;
            
            body.setAttribute("theme", theme);
            localStorage.theme = theme;

            document.getElementById('settings').style.display='none'
        }
    },
    name: 'settings'
}
// @license-end