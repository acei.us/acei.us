// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT
// headerio.js - acei.us - Header Vue component
export default {
    props: ['title'],
    template: /*html*/`<header class="container io-header">
        <nav id="navigation" class="bar">
        <a href="/" class="bar-item button"><img src="/assets/img/logo.png" alt="Cyborg Aceius" width="25"/> Home</a>
        <a class="bar-item"> | </a>
        <div class="dropdown-hover">
            <button class="button">Services</button>
            <div class="dropdown-content bar-block border">
                <a href="https://stuffby.acei.us/cloud" class="bar-item button">Cloud.IO</a>
                <a href="https://stuffby.acei.us/pastebin" class="bar-item button">PrivateBin</a>
                <a href="https://stuffby.acei.us/" class="bar-item button">All Services</a>
            </div>
        </div>
        <!--<a href="https://aceius.gitbook.io/wiki/" class="bar-item button">Wiki</a>
        <div class="dropdown-hover">
            <button class="button">Projects</button>
            <div class="dropdown-content bar-block border">
                <a href="/pages/cloud.html" class="bar-item button">Cloud.IO</a>
                <a href="/pages/cascade.html" class="bar-item button">Cascade</a>
            </div>
        </div>-->
        <a class="bar-item"> | </a>
        <a href="/contact.html" class="bar-item button">Contact</a>
        <a href="/donate.html" class="bar-item button">Donate</a>
        <a href="https://stuffby.acei.us" class="bar-item button right">Portal</a>
        <a href="#" class="bar-item button right" @click="toggle_theme()"><i class="fa-solid fa-circle-half-stroke"></i></a>
        </nav>

        <h1 class="tracking-in-expand">{{title}}</h1>
        <h3>Acei.us</h3>
    </header>`,
    methods: {
        toggle_theme: () => {
            const body = document.getElementById("app")
            if (localStorage.theme == "dark") {
                localStorage.theme = "light";
                body.setAttribute("theme", "light");
                themeicon = localStorage.themeicon = "sun";
                window.location.reload(true);
            } else {
                localStorage.theme = "dark";
                body.setAttribute("theme", "dark");
                themeicon = localStorage.themeicon = "moon";
                window.location.reload(true);
            }		
        }
    },
    data() {
        return {
          themeicon: localStorage.themeicon
        }
    },
    name: 'header-io'
}
// @license-end