// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT
// modal.js - acei.us - Dialog Vue component
export default {
    props: ['title','id'],
    template: /*html*/`<div class="modal" :id="id">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
        <div class="modal-background"></div>
            <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">{{title}}</p>
                <button class="delete" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <slot></slot>
            </section>
            <footer class="modal-card-foot">
                <button class="button is-success">Confirm</button>
                <button class="button">Cancel</button>
            </footer>
        </div>
    </div>`,
    methods: {
        toggle_modal: () => {
            const url = new URL(window.location);
            let title = url.searchParams.get('modal'); 
            if (title != this.id) {
                document.getElementById(this.id).setAttribute("v-hide", "");
            } console.log(this.id)
        }
    },
    name: 'modal'
}
// @license-end