// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT
// headerio.js - acei.us - Footer Vue component
export default {
    template: /*html*/`<footer>
        <div class="ft-gradient"><br><br><br><br></div>
        <div class="io-footer">
            <div class="bar container">
                <a href="/" class="bar-item button">Website:</a>
                <a href="/index.html" class="bar-item button">About</a>
                <a href="/contact.html" class="bar-item button">Contact</a>
                <a href="/pages/rules.html" class="bar-item button">Rules</a>
                <a href="/pages/privacy.html" class="bar-item button">Privacy Policy</a>
            </div>

            <div class="bar container">
                <a href="#" class="bar-item button">Powered by:</a>
                <a href="https://vuejs.org" class="bar-item button"><i class="fa-brands fa-vuejs"></i> Vue.js</a>
                <a href="https://fontawesome.com" class="bar-item button"><i class="fa-solid fa-font-awesome"></i> Font Awesome</a>
                <a class="bar-item"> | </a>
                <a href="#settings" onclick="document.getElementById('settings').style.display='block'; localStorage.modals.settings = true" class="bar-item button"><i class="fa-solid fa-gear"></i> Settings</a>
            </div>

            <div class="container">
                <p>This website's source code is licensed under a <a rel="license" href="https://gitlab.com/AceiusIO/acei.us/-/blob/main/LICENSE">MIT License</a>, unless otherwise noted. If you enjoy the content, please consider <a href="https://acei.us/donate.html">donating</a>. The current stardate is {{stardate}}.</p>
            </div>

            <div class="container io-footer-gradiant">
            <p><a href="https://www.acei.us">www.acei.us</a> made &amp; maintained with love by Aceius.</p>
            </div>
        </div>
    </footer>`,
    computed: {
        stardate: function () {
            const months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
            const date = new Date();
            let staryear = date.getFullYear()
            let starmonth = months[date.getMonth()]
            let timeindex = date.getDate()

            let final_stardate = staryear + '' + starmonth + '.' + timeindex
            return(final_stardate);
        }
    },
    methods: {
        amongus: () => {
            const body = document.getElementById("app");

            body.setAttribute("theme","amongus");
            localStorage.theme="amongus"
            window.setTimeout(function(){
                alert('you a very sussy wussy boi');
            },1);
        }
    },
    name: 'footer-io'
}
// @license-end