// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT
// index.mjs - acei.us - Vue config
import headerio from '/assets/js/components/headerio.js'
import footerio from '/assets/js/components/footerio.js'
import settings from '/assets/js/components/settings.js'
import stardate from '/assets/js/components/datetostardate.js'

const body = document.getElementById("app");
console.info('[Aceius] Welcome to Acei.us!');

function init() {
  console.log('[Theme] Detected theme ' + localStorage.theme);

  if (localStorage.theme == "light") {
    body.setAttribute("theme", "light");
    localStorage.themeicon = "sun"
  } else if (localStorage.theme == "amongus") {
    body.setAttribute("theme", "dark");
    localStorage.theme = "dark"
    localStorage.themeicon = "moon"
  } else {
    body.setAttribute("theme", "dark");
    localStorage.theme = "dark"
    localStorage.themeicon = "moon"
  }
}

function slideshow (sel, ms, func) {
  var i, ss, x = w3.getElements(sel), l = x.length;
  ss = {};
  ss.current = 1;
  ss.x = x;
  ss.ondisplaychange = func;
  if (!isNaN(ms) || ms == 0) {
    ss.milliseconds = ms;
  } else {
    ss.milliseconds = 1000;
  }
  ss.start = function() {
    ss.display(ss.current)
    if (ss.ondisplaychange) {ss.ondisplaychange();}
    if (ss.milliseconds > 0) {
      window.clearTimeout(ss.timeout);
      ss.timeout = window.setTimeout(ss.next, ss.milliseconds);
    }
  };
  ss.next = function() {
    ss.current += 1;
    if (ss.current > ss.x.length) {ss.current = 1;}
    ss.start();
  };
  ss.previous = function() {
    ss.current -= 1;
    if (ss.current < 1) {ss.current = ss.x.length;}
    ss.start();
  };
  ss.display = function (n) {
    w3.styleElements(ss.x, "display", "none");
    w3.styleElement(ss.x[n - 1], "display", "block");
  }
  ss.start();
  return ss;
};

const app = Vue.createApp({
  components: {
      headerio,
      footerio,
      settings,
      stardate
    },
  data() {
    return {
      version: '"Tokyo" 6.0.0-dev',
      greeting: 'Welcome to acei.us'
    }
  }
}).mount('#app')

init();
// @license-end