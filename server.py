import http.server
import socketserver
port = 8080

with socketserver.TCPServer(("", port), http.server.SimpleHTTPRequestHandler) as httpd:
    print("[Server] Opening port", port)
    print("[Server] Document root avaliable at https://localhost, port", port)
    httpd.serve_forever()