# Notice of Archival & Postmortem
Aceius finally learnt Wordpress, and now this repo lives on as Synergy 3 for Wordpress (open source release soon). While this project is great and all, it had no build process resulting in over 2MB of unminified content being sent to the browser. It also failed to capitalise on a proper single file component file making development generally more unpleasing. It looked pretty good though.  
This project was a way for Aceius to learn Vue 2, and they did that. Now they can do much more useful things with proper Vue 3 code, and make better websites with Wordpress and the likes. This website outgrew its orignial purpose, but sometimes all you need is a little HTML.  
An archive of v6.4 is avaliable at https://acei.us/classic.html. v4's archive is located on http://aceius.rf.gd.

The orignial README is disclosed below.

# acei.us
![Discord](https://img.shields.io/discord/807797061181243413?logo=discord&style=for-the-badge)  

[![Stand With Ukraine](https://raw.githubusercontent.com/vshymanskyy/StandWithUkraine/main/badges/StandWithUkraine.svg)](https://stand-with-ukraine.pp.ua)

This repo stores the source code for https://acei.us - Version 6.4.0 Final.  
Feel free to use this as template for your own website. 
This website isn't a full project, it's just a learning opportunity.

## 💾 Project Setup
First, clone the project to disk with the git.  
You will need:
 - Webserver (If you have python one is bundled under `python server.py`)
 - Vue and JS knowledge
 
Once you've made your changes, you can either use it for your own website (provided it's sufficently rebranded), or submit you changes to me via a merge request.
If your changes are deemed stable, they will be pushed to the [staging website](http://staging.acei.us).

## ⚙ Development instructions
 - Keep your code clean
 - Use underscores not camel case or anything like that
 - Use ecma script (ex. `let name;` not `var name;`)
 - Use single quotes
 - When you delete a file, you must name your commit 'Yeeted (filename)'
   - This is a rule not a joke, and will be enforced
     - Stop complaining

## 👥 Our team
Here at Acei.us we have an epic team of pandas with swag who maintain this website!
  - Aceius (founder) - `aceius@acei.us`
  - Emantella (maintainer) - `emantella@staging.acei.us`
  
You can also contact us by emailing
  - `io@acei.us` - General things

Discord: https://acei.us/discord

## 📜 Licencing
Acei.us is licenced under the MIT Licence.